# Source code for l'Aubépine website : www.laubepine.net

# Conseils pour modifications :

## Images

* il est conseillé de mettre les images dans plusieurs formats selon les viewports (tailles/résolutions des appareils) pour optimiser le chargement. De cette manière, le site n'a pas besoin de redimensionner lui-même les images lors du chargement des pages.

* actuellement la gallerie peut présenter trois, deux ou une seule colonne selon la classe utilisée pour l'image (img-row__img__little ou img-row__img_big) et la résolution de l'appareil utilisé (place disponible)