'use strict'

/******** burger/menu logic **************/

function toggleMenu() {
	let burger = document.getElementsByClassName('burger');
	burger[0].addEventListener('click', function (e) {
		let menu = document.getElementsByClassName('nav__menu');
		if (menu[0].style.display != 'block') {
			menu[0].style.display = 'block';
			burger[0].setAttribute("aria-expanded", "true");
		}
		else {
			menu[0].style.display = 'none';
			burger[0].setAttribute("aria-expanded", "false");
		}
	});
}
toggleMenu();


/******** modal: slide show logic ************/

// declare global variables
let currentImage;
let currentImageIndex;

// assign needed html elements to variables
let modal = document.getElementById('modal');
let leftArrow = document.getElementById('modal__content__pane__arrow__left');
let rightArrow = document.getElementById('modal__content__pane__arrow__right');

// add listener on all images with class "img-row__img" to trigger toggleSlideShow
let imgs = document.getElementsByClassName('img-row__img');
imgs = Array.prototype.slice.call(imgs);
imgs.map((image, i) => {
image.addEventListener('click', toggleSlideShow);
});

function goToNextImage() {
	currentImageIndex = currentImageIndex+1
	if (currentImageIndex >= imgs.length) {
		currentImageIndex = 0;
	}
	currentImage.src = imgs[currentImageIndex].src;

}

function goToPreviousImage() {
	currentImageIndex = currentImageIndex-1
	if (currentImageIndex < 0) {
		currentImageIndex = imgs.length-1;
	}
	currentImage.src = imgs[currentImageIndex].src;
}

// listen on keys to exit slideshow or to change image
function keydownWhileInSlideShow(e) {
	// press escape : hide modal
	if (e.keyCode == 27) {
		toggleSlideShow();
	}
	// press left arrow : go to previous image
	if (e.keyCode == 37) {
		goToPreviousImage();
	}
	// press right arrow : go to next image
	if (e.keyCode == 39) {
		goToNextImage();
	}	
}

/******************* MODAL ***************************
****************** display/hide **********************
*********** add/remove document listeners ************
************** add images to slideshow ***************
****************** enable swipe *********************/

function toggleSlideShow(event) {
	if (modal.style.display === 'block') {
		document.removeEventListener('click', checkForClickOutsideOfModal);
		document.removeEventListener('keydown', keydownWhileInSlideShow);
		leftArrow.removeEventListener('click', goToPreviousImage);
		rightArrow.removeEventListener('click', goToNextImage);
		modal.style.display = 'none';
	}
	else {
		document.addEventListener('click', checkForClickOutsideOfModal);
		document.addEventListener('keydown', keydownWhileInSlideShow);
		leftArrow.addEventListener('click', goToPreviousImage);
		rightArrow.addEventListener('click', goToNextImage);
		currentImage = document.getElementById('modal__content__img-current')
		enableSwipeOnSliderForTouchScreen();
		for (let i = 0; i < imgs.length; i++) {
			if (event.target.src == imgs[i].src) {
				currentImageIndex = i;
				currentImage.src = imgs[i].src;
			}
		}
		modal.style.display = 'block';
	}
}

// toggle slideshow if click outside modal
function checkForClickOutsideOfModal(event) {
	if (event.target.className === 'modal__content' || event.target.className === 'modal__content__img' || event.target.className === 'modal__content__pane modal__content__pane__right' || event.target.className === 'modal__content__pane modal__content__pane__left') {
		toggleSlideShow(event);
	}
};

/********** modal: swipe logic for touchscreen devices ************/

let allowedTime = 300;
let touchStartTime;
let elapsedTime;
let distanceThreshold = 130;
let touchStartX;
let touchEndX;
let distanceBetweenTouchStartAndEnd;
let swipeDirection;

function changeImageWhenSwiping(swipeDirection) {
	if (swipeDirection === 'right') {
		goToPreviousImage();
	}
	else if (swipeDirection === 'left'){
		goToNextImage();
	}
}

function getTouchTimeAndPosition(e) {
		let touchedObj = e.changedTouches[0];
		distanceBetweenTouchStartAndEnd = 0;
		touchStartX = touchedObj.pageX;
		touchStartTime = new Date().getTime();
		e.preventDefault();
	}

function checkIfSwipeOrTouch(e) {
		let touchedObj = e.changedTouches[0];
		// distance traveled by fingers between touchStart an TouchEnd
		distanceBetweenTouchStartAndEnd = touchedObj.pageX - touchStartX;
		// time elapsed
		elapsedTime = new Date().getTime() - touchStartTime;
		touchEndX = touchedObj.pageX;
		if (elapsedTime <= 70) {
			toggleSlideShow(e);
		}
		if (elapsedTime > 70 && elapsedTime <= allowedTime) {
			if (distanceBetweenTouchStartAndEnd >= distanceThreshold) {
				swipeDirection = 'right';
				changeImageWhenSwiping(swipeDirection);
			}
			else if (distanceBetweenTouchStartAndEnd <= -distanceThreshold) {
				swipeDirection = 'left';
				changeImageWhenSwiping(swipeDirection);
			}
		}
		e.preventDefault();
	}

function enableSwipeOnSliderForTouchScreen() {
	currentImage.addEventListener('touchstart', getTouchTimeAndPosition, false);
	currentImage.addEventListener('touchmove', (e) => {
		e.preventDefault();
	}, false);
	currentImage.addEventListener('touchend', checkIfSwipeOrTouch, false);
}